#!/bin/perl -w
use Authen::OATH;
use Convert::Base32;
chomp(my $resp = `echo -e "google\nfacebook\nmicrosoft\nteamviewer" | dmenu -b`);
chomp(my $secret = `gpg --decrypt ~/<PATH>/$resp.gpg`);
my $otp = Authen::OATH->new()->totp(  decode_base32( $secret ) );
`xdotool type --clearmodifiers $otp`;
