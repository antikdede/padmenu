#!/bin/perl -w
use Authen::OATH;
use Convert::Base32;
my $secretspath='<PATH>';
chomp(my $secretfile = `ls $secretspath | dmenu -b`);
chomp(my $secret = `gpg --decrypt $secretspath/$secretfile`);
my $otp = Authen::OATH->new()->totp(  decode_base32( $secret ) );
`xdotool type --clearmodifiers $otp`;
